module Model exposing (..)

import Array exposing (Array)
import Json.Decode as D


type alias Model =
    { storedName : Maybe String
    , state : GameState
    }


type GameState
    = Loading
    | NotStarted StartGameForm
    | Join String
    | InGame GameData


type alias StartGameForm =
    { numplayers : Int
    , characters : List Character
    }


type alias Character =
    { description : String
    , fieldname : String
    , checked : Bool
    }


type Msg
    = Recv ServerTopMessage
    | JsonError D.Error
    | StoredName String
    | GotStartGameMsg StartGameMsg
    | GotJoinMsg JoinMsg
    | GotGameMsg GameMsg


type StartGameMsg
    = SetNumPlayers String
    | ToggleCheck String -- For optional characters
    | SubmitStartGame


type JoinMsg
    = EditName String
    | SubmitJoin


type GameMsg
    = GameRecv ServerGameMessage
    | TogglePlayerCheck Int -- For team selection
    | StartVote
    | SendVote Bool
    | SetAssassinationTarget String
    | Assassinate
    | RestartGame


type ServerMessage
    = GotServerTopMessage ServerTopMessage
    | GotServerGameMessage ServerGameMessage


type ServerTopMessage
    = ShowStartForm
    | GameStarted
    | JoinedGame GameData
    | ShowAlert String


type ServerGameMessage
    = SetPlayer Int Player
    | SetLeader Int
    | SetQuestResult Int QuestResult
    | SetPhase GamePhase
    | SetVoteResult VoteResult


type alias VoteResult =
    { approved : Bool
    , team : List Int
    , votes : List Bool
    }


type GamePhase
    = PickTeam Int
    | Vote Int (List Int)
    | Quest (List Int)
    | Assassination
    | GameOver Side String


type Side = Evil | Good


isPickingTeam : GamePhase -> Bool
isPickingTeam phase =
    case phase of
        PickTeam _ -> True
        _ -> False


type alias GameData =
    { localGameState : LocalGameState
    , myId : Int
    , myRole: PlayerRole
    , players : Array (Maybe Player)
    , leader : Int
    , questResults : Array (Maybe QuestResult)
    , phase : GamePhase
    , teamVotes : List VoteResult
    }


type alias LocalGameState =
    { assassinationTarget : Int
    , alreadyVoted : Bool
    }


initGameData = GameData
    { assassinationTarget = 0
    , alreadyVoted = False
    }


type alias QuestResult =
    { succeeded : Bool
    , successNumber : Int
    , failNumber : Int
    , team : Array Int
    }


type alias Player =
    { name : String
    , evil : Bool
    , merlin : Bool
    , checked : Bool
    }


type PlayerRole
    = BasicServant
    | Merlin
    | Percival
    | BasicMinion
    | Assassin
    | Mordred
    | Oberon
    | Morgana
